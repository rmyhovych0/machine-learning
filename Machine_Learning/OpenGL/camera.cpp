#include "camera.h"

Camera::Camera(int width, int height) :
	width_(width),
	height_(height)
{
	projection_ = glm::ortho(0.0f, width_, height_, 0.0f, -1.0f, 1.0f);
	projectionPtr_ = glm::value_ptr(projection_);
}


glm::f32* Camera::getProjectionPtr()
{
	return projectionPtr_;
}