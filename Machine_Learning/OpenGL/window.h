#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "camera.h"

class Window
{
public:
	GLFWwindow* window_;

	Window(int width = 1200, int height = 800);
	~Window();

	void input();
};
