#include "window.h"

double scroll = 0;

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	scroll = yoffset;
}

Window::Window(int width, int height)
{
	//	-- START GLFW --
	glfwInit();

	//	Version 3.2
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

	//	Core profile OpenGL
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//	Forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);


	//	Window creation + OpenGL Context
	window_ = glfwCreateWindow(width, height, "OpenGL", nullptr, nullptr);

	glfwMakeContextCurrent(window_);

	//	Setup GLEW
	glewExperimental = GL_TRUE;
	glewInit();
}

Window::~Window()
{
	//	-- END GLFW --
	glfwTerminate();
}

void Window::input()
{
	if (glfwGetKey(window_, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window_, GL_TRUE);
	}
}
