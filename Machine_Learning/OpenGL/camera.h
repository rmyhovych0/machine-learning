#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include <iostream>

#include <cmath>

#define PI 3.14159265359f

class Camera
{
public:
	Camera(int width, int height);
	
	glm::f32* getProjectionPtr();

private:
	glm::f32* projectionPtr_;

	glm::mat4 projection_;


	float width_;
	float height_;
};

