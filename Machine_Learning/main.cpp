#include <iostream>

#include "population.h"


float fitness(float delta)
{
	return 1/delta;
}

float function(float i, float j)
{
	return i * i + j * j;
}

unsigned xorF(unsigned i, unsigned j)
{
	return i ^ j;
}

int main()
{
	Neat::Population population(2, 1, 150);

	float** inputs = population.getInput();

	float** outputs = population.getOutput();

	while (true)
	{
		population.useNext();


		float delta = 0.0f;

		unsigned count = 0;
		for (float i = -10.0f; i <= 10.0f; i += 10.0f)
		{
			for (float j = -10.0f; j <= 10.0f; j += 10.0f)
			{
				*inputs[0] = i;
				*inputs[1] = j;

				population.play();

				population.addReward(fitness(pow(function(i, j) - *outputs[0], 2)));
			}
		}

		
		



	}
}