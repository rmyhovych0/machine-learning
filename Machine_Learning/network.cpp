
#include "network.h"

using namespace std;


//	CONSTRUCTORS

Network::Network(GlobalData* globalData) :
	id_(globalData->useNetworkID()),
	
	globalData_(globalData),

	adjustedFitness_(0),
	fitness_(0)
{
}


Network::Network(int nInput, int nOutput, GlobalData* globalData) :
	id_(globalData->useNetworkID()),

	globalData_(globalData),

	adjustedFitness_(0),
	fitness_(0)
{
	nodes_.resize(nInput + nOutput + 1);

	nodes_[0] = new Node(BIAS);
	for (unsigned i = 1; i < nInput + 1; i++)
	{
		nodes_[i] = new Node(IN);
	}
	for (unsigned i = nInput + 1; i < nOutput + nInput + 1; i++)
	{
		nodes_[i] = new Node(OUT);
	}


	connexions_.resize((nInput + 1) * nOutput);

	unsigned connexionCounter = 0;
	for (unsigned inNode = 0; inNode < nInput + 1; inNode++)
	{
		for (unsigned outNode = nInput + 1; outNode < nInput + nOutput + 1; outNode++)
		{
			connexions_[connexionCounter] = new Connexion(nodes_[inNode], nodes_[outNode]);
			connexionCounter++;
		}
	}


	for (unsigned node = 0; node < nodes_.size(); node++)
	{
		nodes_[node]->setNumber(node);
	}
	for (unsigned connexion = 0; connexion < connexions_.size(); connexion++)
	{
		connexions_[connexion]->setInnovation(connexion);
	}
	
	globalData_->setInnovation(connexions_.size());
	globalData_->setNodeNumber(nodes_.size());

	generateValues(nInput, nOutput);
}




//	DESTRUCTOR

Network::~Network()
{
	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		delete nodes_[i];
		nodes_[i] = nullptr;
	}

	for (unsigned i = 0; i < connexions_.size(); i++)
	{
		delete connexions_[i];
		connexions_[i] = nullptr;
	}

}




//	GETTERS


unsigned Network::size()
{
	return connexions_.size();
}

float** Network::accessInputValues()
{
	return inValues_;
}

float** Network::accessOutputValues()
{
	return outValues_;
}

float Network::getAdjustedFitness()
{
	return adjustedFitness_;
}

float Network::getFitness()
{
	return fitness_;
}




//	SETTERS






//	OUTPUT


void Network::generateValues(unsigned inSize, unsigned outSize)
{

	if (inValues_ != nullptr)
	{
		delete[] inValues_;
	}
	if (outValues_ != nullptr)
	{
		delete[] outValues_;
	}

	inValues_ = new float*[inSize];
	outValues_ = new float*[outSize];


	unsigned nodeCount = 1;

	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		if (nodes_[i]->getStatus() == IN && 
			nodes_[i]->getNumber() == nodeCount)
		{
			inValues_[nodeCount - 1] = nodes_[i]->accessValue();
			
			nodeCount++;
			i = -1;
		}
	}

	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		if (nodes_[i]->getStatus() == OUT &&
			nodes_[i]->getNumber() == nodeCount)
		{
			outValues_[nodeCount - (inSize + 1)] = nodes_[i]->accessValue();

			nodeCount++;
			i = -1;
		}
	}
}


void Network::out()
{
	for (list<vector<Node*>>::iterator layer = layers_.begin(); layer != layers_.end(); layer++)
	{
		for (unsigned i = 0; i < layer->size(); i++)
		{
			layer->at(i)->calculateValue();
		}
	}
}




//	CROSSOVER

Network* Network::crossover(Network* parent0, Network* parent1)
{
	Network* parents[2] = {parent0, parent1};

	vector<Node*> oldNodes;
	vector<Connexion*> oldConnexions;


	//	finding the most fit parent
	unsigned betterParent = 0;
	unsigned worseParent = 1;

	if (parents[0]->adjustedFitness_ < parents[1]->adjustedFitness_)
	{
		betterParent = 1;
		worseParent = 0;
	}


	//	adding all best parent connexions
	for (unsigned i = 0; i < parents[betterParent]->connexions_.size(); i++)
	{
		oldConnexions.push_back(parents[betterParent]->connexions_[i]);
	}


	//	randomly chosing the same connexion from both parents
	for (unsigned nWorseParentConnexion = 0; nWorseParentConnexion < parents[worseParent]->connexions_.size(); nWorseParentConnexion++)
	{
		for (unsigned nOldConnexion = 0; nOldConnexion < oldConnexions.size(); nOldConnexion++)
		{
			if (*parents[worseParent]->connexions_[nWorseParentConnexion] == *oldConnexions[nOldConnexion])
			{
				innovation_t sameConnexionInnovation = oldConnexions[nOldConnexion]->getInnovation();

				//	chosing random connexion
				oldConnexions[nOldConnexion] = parents[randomInt(2)]->accessConnexion(sameConnexionInnovation);
			}
		}
	}


	//	add old nodes and link their indexes to old connexions
	vector<pair<unsigned, unsigned>> nodeConnexionIndexes;

	for (unsigned nOldConnexion = 0; nOldConnexion < oldConnexions.size(); nOldConnexion++)
	{
		pair<unsigned, unsigned> nodeConnexionIndex(-1, -1);


		//	setting indexes for already added nodes
		for (unsigned nOldNode = 0; nOldNode < oldNodes.size(); nOldNode++)
		{
			if (*oldConnexions[nOldConnexion]->getInputNode() == *oldNodes[nOldNode])
			{
				nodeConnexionIndex.first = nOldNode;
			}

			if (*oldConnexions[nOldConnexion]->getOutputNode() == *oldNodes[nOldNode])
			{
				nodeConnexionIndex.second = nOldNode;
			}
		}


		//	adding not added nodes
		if (nodeConnexionIndex.first == -1)
		{
			oldNodes.push_back(oldConnexions[nOldConnexion]->getInputNode());
			nodeConnexionIndex.first = oldNodes.size() - 1;
		}

		if (nodeConnexionIndex.second == -1)
		{
			oldNodes.push_back(oldConnexions[nOldConnexion]->getOutputNode());
			nodeConnexionIndex.second = oldNodes.size() - 1;
		}


		//	adding indexes for each connexion
		nodeConnexionIndexes.push_back(nodeConnexionIndex);
	}


	//	creating new Network with any parent's GlobalData
	Network* newNetworkPtr = new Network(parents[0]->globalData_);


	//	copying old nodes to the new Network
	for (unsigned i = 0; i < oldNodes.size(); i++)
	{
		newNetworkPtr->nodes_.push_back(new Node(*oldNodes[i]));
	}


	//	copying old connexions to the new Network
	for (unsigned i = 0; i < oldConnexions.size(); i++)
	{
		newNetworkPtr->connexions_.push_back(new Connexion(*oldConnexions[i]));

		//	connecting new nodes with new connexions
		newNetworkPtr->connexions_[i]->setInputNode(newNetworkPtr->nodes_[nodeConnexionIndexes[i].first]);
		newNetworkPtr->connexions_[i]->setOutputNode(newNetworkPtr->nodes_[nodeConnexionIndexes[i].second]);
	}
	
	return newNetworkPtr;
}




//	CONNEXION INDEXING

Connexion* Network::accessConnexion(innovation_t index)
{
	for (unsigned i = 0; i < connexions_.size(); i++)
	{
		if (index == connexions_[i]->getInnovation())
		{
			return connexions_[i];
		}
	}

	return nullptr;
}




//	MUTATION

void Network::mutateWeights()
{
	for (unsigned i = 0; i < connexions_.size(); i++)
	{
		
		if (chance(WEIGHT_MUTATION_CHANCE))
		{
			connexions_[i]->mutate();
		}

	}
}


void Network::mutateTopology()
{
	if (chance(TOPOLOGY_MUTATION_CHANCE))
	{
		if (chance(NEW_NODE_CHANCE))
		{
			addNode();
		}
		else
		{
			addConnexion();
		}
	}
}




//	FITNESS

void Network::addFitness(float fitness)
{
	fitness_ += fitness;
}


void Network::adjustFitness(unsigned speciesSize)
{
	adjustedFitness_ = fitness_ / speciesSize;
}



void Network::clearFitness()
{
	fitness_ = 0;
	adjustedFitness_ = 0;
}




//	LAYER GENERATION

void Network::generateNetwork()
{
	//	flush layers
	layers_.clear();

	//	flush layer values
	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		nodes_[i]->setLayer(-1);
	}

	//	input nodes as first layer
	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		if (nodes_[i]->getStatus() == IN || nodes_[i]->getStatus() == BIAS)
		{
			nodes_[i]->setLayer(0);
		}
	}

	//	find layers for every node
	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		for (unsigned j = 0; j < nodes_.size(); j++)
		{
			if (nodes_[i]->hasIn(nodes_[j]) && 
				nodes_[i]->getLayer() <= nodes_[j]->getLayer())
			{
				nodes_[i]->setLayer(nodes_[j]->getLayer() + 1);

				i = 0;
				j = nodes_.size();
			}
		}
	}


	int nCompletedNodes = 0;

	vector<Node*> layer;

	//	create layers and push them to layers_
	while (nCompletedNodes != nodes_.size())
	{
		for (unsigned i = 0; i < nodes_.size(); i++)
		{
			if (nodes_[i]->getLayer() == layers_.size())
			{
				layer.push_back(nodes_[i]);
				nCompletedNodes++;
			}
		}

		layers_.push_back(layer);
		layer.clear();
	}

	layers_.pop_front();

}




//	SPECIATION


float Network::compatibilityDistance(Network* network)
{
	float compatibility = 0;

	compatibility += FACTOR_EXCESS * nExcessGenes(network);
	compatibility += FACTOR_DISJOINT * nDisjointGenes(network);

	compatibility /= max(this->size(), network->size());

	compatibility += FACTOR_WEIGHT_DIFFERENCE * averageWeightDifference(network);

	return compatibility;
}




//	TOPOLOGY MUTATION


void Network::addConnexion()
{
	vector<pair<int, int>> possibleConnexions;

	for (unsigned i = 0; i < nodes_.size(); i++)
	{
		if (nodes_[i]->getStatus() != IN && nodes_[i]->getStatus() != BIAS)
		{
			for (unsigned j = i + 1; j < nodes_.size(); j++)
			{
				if (nodes_[i]->getLayer() > nodes_[j]->getLayer() && !(nodes_[i]->hasIn(nodes_[j])))
				{
					possibleConnexions.push_back(pair<int, int>(j, i));
				}
			}
		}
	}

	if (possibleConnexions.size() != 0)
	{
		int pairIndex = randomInt(possibleConnexions.size());

		Connexion* newConnexion = new Connexion(nodes_[possibleConnexions[pairIndex].first], nodes_[possibleConnexions[pairIndex].second]);
		
		connexions_.push_back(newConnexion);
		*globalData_ += newConnexion;
	}
}


void Network::addNode()
{
	int splittingIndex = randomInt(connexions_.size());
	
	Node* newNode = new Node(HID);

	Connexion* newInConnexion = new Connexion(connexions_[splittingIndex]->getInputNode(), newNode, 1);
	Connexion* newOutConnexion = new Connexion(newNode, connexions_[splittingIndex]->getOutputNode(), connexions_[splittingIndex]->getWeight());

	
	nodes_.push_back(newNode);

	connexions_.push_back(newInConnexion);
	connexions_.push_back(newOutConnexion);

	*globalData_ += pair<Node*, Connexion*>(newNode, connexions_[splittingIndex]);

	connexions_.erase(connexions_.begin() + splittingIndex);
}




//	CONNEXION CHECKING


bool Network::has(Connexion* connexion)
{
	for (unsigned i = 0; i < connexions_.size(); i++)
	{
		if (*connexions_[i] == *connexion)
		{
			return true;
		}
	}

	return false;
}




//	COMPATIBILITY CHECKING


innovation_t Network::maxInnovation()
{
	innovation_t innovation = 0;

	for (unsigned i = 0; i < connexions_.size(); i++)
	{
		if (innovation < connexions_[i]->getInnovation())
		{
			innovation = connexions_[i]->getInnovation();
		}
	}

	return innovation;
}


unsigned Network::nExcessGenes(Network* network)
{
	innovation_t maxInnThis = this->maxInnovation();
	innovation_t maxInnNet = network->maxInnovation();

	unsigned count = 0;
	
	if (maxInnNet > maxInnThis)
	{
		for (innovation_t i = maxInnThis + 1; i < maxInnNet; i++)
		{
			if (network->accessConnexion(i) != nullptr)
			{
				count++;
			}
		}
	}

	else if (maxInnNet < maxInnThis)
	{
		for (innovation_t i = maxInnNet + 1; i < maxInnThis; i++)
		{
			if (this->accessConnexion(i) != nullptr)
			{
				count++;
			}
		}
	}

	return count;
}


unsigned Network::nDisjointGenes(Network* network)
{
	innovation_t smallestMaxInnovation = min(network->maxInnovation(), this->maxInnovation());

	unsigned count = 0;

	for (innovation_t i = 0; i < smallestMaxInnovation; i++)
	{
		if (network->accessConnexion(i) != nullptr && this->accessConnexion(i) == nullptr)
		{
			count++;
		}
		
		else if (network->accessConnexion(i) == nullptr && this->accessConnexion(i) != nullptr)
		{
			count++;
		}
	}

	return count;
}


unsigned Network::nMatchingGenes(Network* network)
{
	unsigned count = 0;

	for (unsigned i = 0; i < connexions_.size(); i++)
	{
		for (unsigned j = 0; j < network->connexions_.size(); j++)
		{
			if (*connexions_[i] == *network->connexions_[j])
			{
				count++;
			}
		}
	}

	return count;
}



float Network::averageWeightDifference(Network* network)
{
	float difference = 0;

	for (innovation_t i = 0; i < this->maxInnovation(); i++)
	{
		Connexion* connexion0 = this->accessConnexion(i);
		Connexion* connexion1 = network->accessConnexion(i);

		if (connexion0 != nullptr && 
			connexion1 != nullptr)
		{
			difference += abs(connexion1->getWeight() - connexion0->getWeight());
		}
	}

	return difference / this->nMatchingGenes(network);
}
