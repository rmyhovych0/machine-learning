#include "elements.h"

using namespace std;


////////////////////
////    NODE    ////
////////////////////



//	CONSTRUCTORS


Node::Node() :
	number_(-1),
	value_(-1),
	layer_(-1),
	status_(HID)
{
}


Node::Node(status_e status) :
	number_(-1),
	value_(-1),
	layer_(-1),
	status_(status)
{
}


Node::Node(Node& node) :
	number_(node.number_),
	value_(-1),
	layer_(-1),
	status_(node.status_)
{
}




//	VALUE OPERATIONS


float* Node::accessValue()
{
	return &value_;
}


void Node::calculateValue()
{
	value_ = 0;
	for (unsigned i = 0; i < inConnexions_.size(); i++)
	{
		if (inConnexions_[i]->isEnabled())
		{
			inConnexions_[i]->passValue();
		}
	}
	if (status_ != OUT)
	{
		sigmoid();
	}
}




//	GETTERS


unsigned Node::getNumber()
{
	return number_;
}


float Node::getValue()
{
	return value_;
}


int Node::getLayer()
{
	return layer_;
}


status_e Node::getStatus()
{
	return status_;
}



Connexion* Node::accessInput(unsigned index)
{
	return inConnexions_[index];
}


Connexion* Node::accessOutput(unsigned index)
{
	return outConnexions_[index];
}




//	SETTERS


void Node::setNumber(unsigned number)
{
	number_ = number;
}


void Node::setValue(float value)
{
	value_ = value;
}


void Node::setLayer(int layer)
{
	layer_ = layer;
}




//	CONNEXIONS


Node& Node::connectIn(Connexion* connexion)
{
	inConnexions_.push_back(connexion);
	return *this;
}


Node& Node::connectOut(Connexion* connexion)
{
	outConnexions_.push_back(connexion);
	return *this;
}




//	OPERATORS


Node& Node::operator+=(float value)
{
	value_ += value;
	return *this;
}


Node& Node::operator=(Node& node)
{
	if (this != &node)
	{
		number_ = node.number_;
	}
	return *this;
}


bool Node::operator==(Node& node)
{
	return (number_ == node.number_);
}




//	LINKED NODES


bool Node::hasIn(Node* node)
{
	for (unsigned i = 0; i < inConnexions_.size(); i++)
	{
		if (inConnexions_[i]->getInputNode() == node)
		{
			return true;
		}
	}
	return false;
}




//	PRIVATE METHODS


void Node::sigmoid()
{
	value_ = 1 / (1 + exp(-value_));
	//if (value_ < 0)
	//{
	//	value_ = 0;
	//}
}





/////////////////////////
////    CONNEXION    ////
/////////////////////////



//	CONSTRUCTORS 


Connexion::Connexion(Node* inNode, Node* outNode) :
	innovation_(-1),
	enabled_(true),

	inNode_(inNode),
	outNode_(outNode),

	weight_(0)
{
	randomNewWeight(weight_);
	outNode->connectIn(this);
	inNode->connectOut(this);
}


Connexion::Connexion(Node* inNode, Node* outNode, float weight):
	innovation_(-1),
	enabled_(true),

	inNode_(inNode),
	outNode_(outNode),

	weight_(weight)
{
	outNode->connectIn(this);
	inNode->connectOut(this);
}


Connexion::Connexion(Connexion& connexion) :
	innovation_(connexion.innovation_),
	enabled_(connexion.enabled_),

	inNode_(nullptr),
	outNode_(nullptr),

	weight_(connexion.weight_)
{
}




//	VALUE OPERATIONS


void Connexion::passValue()
{
	(*outNode_) += inNode_->getValue() * weight_;
}




//	GETTERS

innovation_t Connexion::getInnovation()
{
	return innovation_;
}


bool Connexion::isEnabled()
{
	return enabled_;
}



Node* Connexion::getInputNode()
{
	return inNode_;
}


Node* Connexion::getOutputNode()
{
	return outNode_;
}



float Connexion::getWeight()
{
	return weight_;
}




//	SETTERS

void Connexion::setInnovation(innovation_t innovation)
{
	innovation_ = innovation;
}



void Connexion::enable()
{
	enabled_ = true;
}


void Connexion::disable()
{
	enabled_ = false;
}



void Connexion::setInputNode(Node* node)
{
	if (this->inNode_ != nullptr)
	{
		delete this->inNode_;
	}

	inNode_ = node;
	node->connectOut(this);
}


void Connexion::setOutputNode(Node* node)
{
	if (this->outNode_ != nullptr)
	{
		delete this->outNode_;
	}

	outNode_ = node;
	node->connectIn(this);
}



void Connexion::mutate()
{
	if (chance(WEIGHT_RANDOM_ASSIGNEMENT_CHANCE))
	{
		randomNewWeight(weight_);
	}
	else
	{
		randomModifyWeight(weight_);
	}
}




//	OPERATORS

Connexion& Connexion::operator=(Connexion& connexion)
{
	if (this != &connexion)
	{
		innovation_ = connexion.innovation_;
		enabled_ = connexion.enabled_;
	}
	return *this;
}



bool Connexion::operator==(Connexion& connexion)
{
	return (this->innovation_ == connexion.innovation_);
}





///////////////////////////
////    GLOBAL_DATA    ////
///////////////////////////



//	CONSTRUCTOR


GlobalData::GlobalData(unsigned nodeNumber, innovation_t innovation) :
	networkID_(0),
	nodeNumber_(nodeNumber),
	innovation_(innovation)
{
}




//	DESTRUCTOR

GlobalData::~GlobalData()
{
	for (unsigned i = 0; i < addedConnexions_.size(); i++)
	{
		delete addedConnexions_[i];
		addedConnexions_[i] = nullptr;
	}

	for (unsigned i = 0; i < addedNodes_.size(); i++)
	{
		delete addedNodes_[i].first;
		addedNodes_[i].first = nullptr;

		delete addedNodes_[i].second;
		addedNodes_[i].second = nullptr;
	}
}




//	GETTERS

innovation_t GlobalData::getInnovation()
{
	return innovation_;
}


unsigned GlobalData::getNodeNumber()
{
	return nodeNumber_;
}



unsigned GlobalData::useNetworkID()
{
	return networkID_++;
}




//	SETTERS

void GlobalData::setInnovation(innovation_t innovation)
{
	innovation_ = innovation;
}


void GlobalData::setNodeNumber(unsigned nodeNumber)
{
	nodeNumber_ = nodeNumber;
}



void GlobalData::clear()
{
	addedConnexions_.clear();
}




//	OPERATORS

GlobalData& GlobalData::operator+=(Connexion* connexion)
{
	for (unsigned i = 0; i < addedConnexions_.size(); i++)
	{
		if (*addedConnexions_[i]->getOutputNode() == *connexion->getOutputNode() &&
			*addedConnexions_[i]->getInputNode() == *connexion->getInputNode())
		{
			connexion->setInnovation(addedConnexions_[i]->getInnovation());

			return *this;
		}
	}

	connexion->setInnovation(innovation_++);

	Connexion* connexionCopy = new Connexion(*connexion);

	connexionCopy->setInputNode(new Node(*connexion->getInputNode()));
	connexionCopy->setOutputNode(new Node(*connexion->getOutputNode()));

	addedConnexions_.push_back(connexionCopy);

	return *this;
}


GlobalData& GlobalData::operator+=(nodePair ncPair)
{
	Node* newNode = ncPair.first;

	for (unsigned addedNcPair = 0; addedNcPair < addedNodes_.size(); addedNcPair++)
	{
		if (*addedNodes_[addedNcPair].second == *ncPair.second)
		{
			*newNode = *addedNodes_[addedNcPair].first;

			*this += newNode->accessInput(0);
			*this += newNode->accessOutput(0);

			return *this;

		}
	}

	newNode->setNumber(nodeNumber_++);

	*this += newNode->accessInput(0);
	*this += newNode->accessOutput(0);

	nodePair ncPairCopy;
	ncPairCopy.first = new Node(*ncPair.first);
	ncPairCopy.second = new Connexion(*ncPair.second);

	addedNodes_.push_back(ncPairCopy);

	return *this;
}





////////////////////////////////
////    GLOBAL FUNCTIONS    ////
////////////////////////////////


//	GLOBAL VARIABLES

default_random_engine generator;
normal_distribution<float> randomAssignement(0, 0.01);
normal_distribution<float> uniformPerturbation(0, 0.00001);


int randomInt(int nPossibilities)
{
	return (rand() % nPossibilities);
}


bool chance(float percentage)
{
	if (((float)rand()) / ((float)RAND_MAX) <= percentage)
	{
		return true;
	}
	return false;
}


void randomNewWeight(float& weight)
{
	weight = randomAssignement(generator);
}


void randomModifyWeight(float& weight)
{
	weight += (2*randomInt(2) - 1) * 0.001;//uniformPerturbation(generator);
}
