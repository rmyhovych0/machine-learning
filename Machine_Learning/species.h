#pragma once

#include "network.h"


const float RANDOM_PARENT_CHANCE = 0.001f;


class Species :
	public vector<unsigned>
{
public:

	Species(unsigned representative);

	unsigned getRepresentative();

	void reset();

private:
	
	unsigned lifespan_;

	unsigned representative_;
};

