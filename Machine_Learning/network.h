#pragma once

#include <list>

#include "elements.h"


const float WEIGHT_MUTATION_CHANCE = 0.8f;

const float TOPOLOGY_MUTATION_CHANCE = 0.1f;
const float NEW_NODE_CHANCE = 0.6f;

const float FACTOR_EXCESS = 1.0f;
const float FACTOR_DISJOINT = 1.0f;
const float FACTOR_WEIGHT_DIFFERENCE = 0.4f;
 

class Network
{
public:

	//	CONSTRUCTORS

	Network(GlobalData* globalData);
	Network(int nInput, int nOutput, GlobalData* globalData);



	//	DESTRUCTOR

	~Network();



	//	GETTERS

	unsigned size();

	float** accessInputValues();
	float** accessOutputValues();

	float getAdjustedFitness();
	float getFitness();



	//	OUTPUT

	void generateValues(unsigned inSize, unsigned outSize);
	
	void out();



	//	CROSSOVER

	static Network* crossover(Network* parent0, Network* parent1);



	//	CONNEXION INDEXING

	Connexion* accessConnexion(innovation_t index);



	//	MUTATION

	void mutateWeights();
	void mutateTopology();



	//	FITNESS

	void addFitness(float fitness);
	void adjustFitness(unsigned speciesSize);

	void clearFitness();



	//	LAYER GENERATION

	void generateNetwork();



	//	SPECIATION

	float compatibilityDistance(Network* network);


private:

	//	TOPOLOGY MUTATION

	void addConnexion();
	void addNode();



	//	CONNEXION CHECKING

	bool has(Connexion* connexion);



	//	COMPATIBILITY CHECKING

	innovation_t maxInnovation();
	
	unsigned nExcessGenes(Network* network);
	unsigned nDisjointGenes(Network* network);
	unsigned nMatchingGenes(Network* network);

	float averageWeightDifference(Network* network);

private:

	//	ATTRIBUTES

	unsigned id_;

	vector<Node*> nodes_;
	vector<Connexion*> connexions_;
	list<vector<Node*>> layers_;

	float** inValues_;
	float** outValues_;

	GlobalData* globalData_;

	float adjustedFitness_;
	float fitness_;
};

