
#include "species.h"
#include "network.h"

#include "population.h"

#include <iostream>

float THRESHOLD_COMPATIBILITY = 3.0f;


Neat::Population::Population(unsigned inSize, unsigned outSize, unsigned nNetworks) :
	NUMBER_BORN_DIED(nNetworks / 20),
	bestFitness_(0),
	populationSize_(nNetworks),
	generation_(0),

	globalData_(GlobalData((inSize + 1) + outSize, (inSize + 1) * outSize)),

	inSize_(inSize),
	outSize_(outSize),

	input_(new float*[inSize]),
	output_(new float*[outSize])
{
	for (unsigned i = 0; i < nNetworks; i++)
	{
		networks_.push_back(new Network(inSize_, outSize_, &globalData_));
	}

	species_.push_back(new Species(0));

	speciatePopulation();

	usedNetwork_ = networks_.begin();

	setData();
}






Neat::Population::~Population()
{
	for (unsigned i = 0; i < networks_.size(); i++)
	{
		delete networks_[i];
		networks_[i] = nullptr;
	}

	for (unsigned i = 0; i < species_.size(); i++)
	{
		delete species_[i];
		species_[i] = nullptr;
	}

	delete[] input_;
	delete[] output_;
}






void Neat::Population::addReward(float reward)
{
	Network* network = *usedNetwork_;

	network->addFitness(reward);

}





void Neat::Population::resetPopulation()
{
	nextGeneration();

	speciatePopulation();

	resetNetworks();
}






float** Neat::Population::getInput()
{
	return input_;
}





float** Neat::Population::getOutput()
{
	return output_;
}






unsigned Neat::Population::getGeneration()
{
	return generation_;
}






void Neat::Population::play()
{
	(*usedNetwork_)->out();
}






void Neat::Population::useNext()
{
	if ((*usedNetwork_)->getFitness() > bestFitness_)
	{
		bestFitness_ = (*usedNetwork_)->getFitness();
	}

	(*usedNetwork_)->mutateWeights();
	(*usedNetwork_)->mutateTopology();

	usedNetwork_++;

	if (usedNetwork_ == networks_.end())
	{
		cout << "Generation : " << generation_ << " ->\t" << bestFitness_ << "\n\n\n\n";

		resetPopulation();

		usedNetwork_ = networks_.begin();

		generation_++;
	}

	setData();
}





void Neat::Population::setData()
{
	Network* network = *usedNetwork_;

	network->generateNetwork();

	float** inputValues = network->accessInputValues();

	for (unsigned i = 0; i < inSize_; i++)
	{
		input_[i] = inputValues[i];
	}


	float** outputValues = network->accessOutputValues();

	for (unsigned i = 0; i < inSize_; i++)
	{
		output_[i] = outputValues[i];
	}
}











void Neat::Population::nextGeneration()
{

	adjustFitnesses();


	for (unsigned i = 0; i < NUMBER_BORN_DIED; i++)
	{
		Network* newNetwork = speciesCrossover(randomInt(species_.size()));

		newNetwork->generateValues(inSize_, outSize_);

		networks_.push_back(newNetwork);
	}

	for (unsigned i = 0; i < NUMBER_BORN_DIED; i++)
	{
		unsigned speciesIndex = randomInt(species_.size());

		unsigned worstIndex = bottomSpeciesNetwork(speciesIndex);

		if (species_[speciesIndex]->size() == 1)
		{
			species_.erase(species_.begin() + speciesIndex);
		}

		networks_.erase(networks_.begin() + worstIndex);
	}
}







Network* Neat::Population::speciesCrossover(unsigned speciesIndex)
{
	unsigned parent0;
	unsigned parent1;

	speciesParents(speciesIndex, parent0, parent1);

	Network* newNetwork = Network::crossover(networks_[parent0], networks_[parent1]);

	return newNetwork;
}







void Neat::Population::speciesParents(unsigned speciesIndex, unsigned& parent0, unsigned& parent1)
{
	unsigned best0 = top0SpeciesNetwork(speciesIndex);
	unsigned best1 = top1SpeciesNetwork(speciesIndex);

	//	parent 0
	if (chance(RANDOM_PARENT_CHANCE))
	{
		parent0 = randomInt(species_[speciesIndex]->size());
	}
	else
	{
		parent0 = best0;
	}


	//	parent 1
	if (chance(RANDOM_PARENT_CHANCE))
	{
		//	mitosis sometimes
		parent1 = randomInt(species_[speciesIndex]->size());
	}
	else
	{
		if (parent0 == best0)
		{
			parent1 = best1;
		}
		else
		{
			parent1 = best0;
		}
	}

	parent0 = species_[speciesIndex]->at(parent0);
	parent1 = species_[speciesIndex]->at(parent1);

}







unsigned Neat::Population::top0SpeciesNetwork(unsigned speciesIndex)
{
	unsigned index = 0;

	for (unsigned nNetwork = 1; nNetwork < species_[speciesIndex]->size(); nNetwork++)
	{
		if (networks_[species_[speciesIndex]->at(index)]->getAdjustedFitness() < 
			networks_[species_[speciesIndex]->at(nNetwork)]->getAdjustedFitness())
		{
			index = nNetwork;
		}
	}

	return index;
}





unsigned Neat::Population::top1SpeciesNetwork(unsigned speciesIndex)
{
	unsigned indexes[2];

	indexes[0] = top0SpeciesNetwork(speciesIndex);

	if (species_[speciesIndex]->size() == 1)
	{
		return indexes[0];
	}

	indexes[1] = (indexes[0] + 1) % species_[speciesIndex]->size();

	for (unsigned nNetwork = species_[speciesIndex]->at(1); nNetwork < species_[speciesIndex]->size(); nNetwork++)
	{
		if (indexes[0] != nNetwork &&
			networks_[species_[speciesIndex]->at(indexes[1])]->getAdjustedFitness() < 
			networks_[species_[speciesIndex]->at(nNetwork)]->getAdjustedFitness())
		{
			indexes[1] = nNetwork;
		}
	}

	return indexes[1];
}







unsigned Neat::Population::bottomSpeciesNetwork(unsigned speciesIndex)
{
	unsigned index = 0;

	for (unsigned nNetwork = 0; nNetwork < species_[speciesIndex]->size(); nNetwork++)
	{
		if (networks_[species_[speciesIndex]->at(index)]->getAdjustedFitness() >
			networks_[species_[speciesIndex]->at(nNetwork)]->getAdjustedFitness())
		{
			index = nNetwork;
		}
	}

	return index;
}






void Neat::Population::speciatePopulation()
{
	resetSpecies();

	for (unsigned nNetwork = 0; nNetwork < networks_.size(); nNetwork++)
	{
		Network* network = networks_[nNetwork];

		bool added = false;

		for (unsigned nSpecies = 0; nSpecies < species_.size(); nSpecies++)
		{
			Network* representative = networks_[species_[nSpecies]->getRepresentative()];

			if (network != representative)
			{
				if (representative->compatibilityDistance(network) <= THRESHOLD_COMPATIBILITY)
				{
					species_[nSpecies]->push_back(nNetwork);

					added = true;

					break;
				}
			}
			else
			{
				added = true;

				break;
			}
		}

		if (!added)
		{
			species_.push_back(new Species(nNetwork));
		}
	}

	if (species_.size() > NUMBER_BORN_DIED)
	{
		THRESHOLD_COMPATIBILITY *= 1.1;
	}
}





void Neat::Population::resetSpecies()
{
	for (unsigned i = 0; i < species_.size(); i++)
	{
		species_[i]->reset();
	}
}





void Neat::Population::resetNetworks()
{
	for (unsigned i = 0; i < networks_.size(); i++)
	{
		networks_[i]->clearFitness();
	}
}





unsigned Neat::Population::getSmallestSpeciesIndex()
{
	unsigned index = 0;

	for (unsigned i = 1; i < species_.size(); i++)
	{
		if (species_[index]->size() > species_[i]->size())
		{
			index = i;
		}
	}

	return index;
}







void Neat::Population::adjustFitnesses()
{
	for (unsigned i = 0; i < species_.size(); i++)
	{
		for (unsigned j = 0; j < species_[i]->size(); j++)
		{
			networks_[species_[i]->at(j)]->adjustFitness(species_[i]->size());
		}
	}
}





void Neat::Population::fillNetworkGap(Network* network)
{
	for (unsigned nNetwork = 0; nNetwork < networks_.size(); nNetwork++)
	{
		if (networks_[nNetwork] == nullptr)
		{
			networks_[nNetwork] = network;

			break;
		}
	}
}
