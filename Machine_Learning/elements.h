#pragma once

#include <vector>
#include <random>

using namespace std;

typedef unsigned innovation_t;

enum status_e { BIAS, IN, HID, OUT };


const float WEIGHT_RANDOM_ASSIGNEMENT_CHANCE = 0.1f;


class Connexion;

////////////
//  NODE  //
////////////

class Node
{
public:

	//	CONSTRUCTORS

	Node();
	Node(status_e status);
	Node(Node& node);



	//	VALUE OPERATIONS

	float* accessValue();
	void calculateValue();



	//	GETTERS

	unsigned getNumber();
	float getValue();
	int getLayer();
	status_e getStatus();

	Connexion* accessInput(unsigned index);
	Connexion* accessOutput(unsigned index);



	//	SETTERS

	void setNumber(unsigned number);
	void setValue(float value);
	void setLayer(int layer);



	//	CONNEXIONS

	Node& connectIn(Connexion* connexion);
	Node& connectOut(Connexion* connexion);



	//	OPERATORS

	Node& operator+=(float value);

	Node& operator=(Node& node);
	bool operator==(Node& node);



	//	LINKED NODES

	bool hasIn(Node* node);


private:

	//	PRIVATE METHODS

	void sigmoid();




private:

	//	ATTRIBUTES

	unsigned number_;

	int layer_;
	status_e status_;

	vector<Connexion*> inConnexions_;
	vector<Connexion*> outConnexions_;

	float value_;
};


/////////////////
//  CONNEXION  //
/////////////////

class Connexion
{
public:
	
	//	CONSTRUCTORS 
	
	Connexion(Node* inNode, Node* outNode);
	Connexion(Node* inNode, Node* outNode, float weight);
	Connexion(Connexion& connexion);



	//	VALUE OPERATIONS

	void passValue();



	//	GETTERS
	
	innovation_t getInnovation();
	bool isEnabled();

	Node* getInputNode();
	Node* getOutputNode();
	
	float getWeight();



	//	SETTERS

	void setInnovation(innovation_t innovation);

	void enable();
	void disable();

	void setInputNode(Node* node);
	void setOutputNode(Node* node);

	void mutate();



	//	OPERATORS
	
	Connexion& operator=(Connexion& connexion);
	
	bool operator==(Connexion& connexion);


private:

	//	ATTRIBUTES

	innovation_t innovation_;
	bool enabled_;

	Node* inNode_;
	Node* outNode_;

	float weight_;
};


///////////////////
//  GLOBAL_DATA  //
///////////////////

typedef std::pair<Node*, Connexion*> nodePair;
typedef std::vector<nodePair> nodePairVector;

class GlobalData
{
public:

	//	CONSTRUCTOR

	GlobalData(unsigned nodeNumber = 0, innovation_t innovation = 0);



	//	DESTRUCTOR

	~GlobalData();



	//	GETTERS

	innovation_t getInnovation();
	unsigned getNodeNumber();

	unsigned useNetworkID();



	//	SETTERS

	void setInnovation(innovation_t innovation);
	void setNodeNumber(unsigned nodeNumber);

	void clear();



	//	OPERATORS

	GlobalData& operator+=(Connexion* connexion);
	GlobalData& operator+=(nodePair ncPair);


private:

	//	ATTRIBUTES

	unsigned networkID_;

	innovation_t innovation_;
	unsigned nodeNumber_;

	vector<Connexion*> addedConnexions_;
	vector<pair<Node*, Connexion*>> addedNodes_;
};


////////////////////////
//  GLOBAL FUNCTIONS  //
////////////////////////

int randomInt(int nPossibilities);

bool chance(float percentage);

void randomNewWeight(float& weight);

void randomModifyWeight(float& weight);