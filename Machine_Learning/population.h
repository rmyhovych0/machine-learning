#pragma once

#include "species.h"

namespace Neat
{
	class Population
	{
	public:
		Population(unsigned inSize, unsigned outSize, unsigned nNetworks = 100);
		~Population();

		void addReward(float reward);

		float** getInput();
		float** getOutput();

		unsigned getGeneration();

		void play();

		void useNext();

	private:

		void resetPopulation();

		void setData();

		void nextGeneration();


		Network* speciesCrossover(unsigned speciesIndex);

		void speciesParents(unsigned speciesIndex, unsigned& parent0, unsigned& parent1);

		unsigned top0SpeciesNetwork(unsigned speciesIndex);
		unsigned top1SpeciesNetwork(unsigned speciesIndex);

		unsigned bottomSpeciesNetwork(unsigned speciesIndex);

		void speciatePopulation();

		void resetSpecies();

		void resetNetworks();

		unsigned getSmallestSpeciesIndex();

		void adjustFitnesses();

		void fillNetworkGap(Network* network);

	private:

		const unsigned NUMBER_BORN_DIED;

		float bestFitness_;

		unsigned generation_;
		
		unsigned populationSize_;

		GlobalData globalData_;

		unsigned inSize_;
		unsigned outSize_;

		vector<Network*>::iterator usedNetwork_;

		vector<Network*> networks_;
		vector<Species*> species_;

		float** input_;
		float** output_;
	};

}
